$(document).on('click', '.dropdown-notifications', function (e) {
    e.stopPropagation();
});

function newsuccessFunction(data) {
    /* do something here */
    $('div.notifications').empty();
    $('div.allnotifications').empty();
    $('#ntcounttit').text(data.not_count);
    $('#ntcountbdg').text(data.not_count);
    if(parseInt(data.not_count)==0)$('div.allnotifications').append('<p>Nemáte žádná nová upozornění</p>');
    $.each(JSON.parse(data.notifications),function(i, notification){
        $('div.notifications').append( createNotificationDropdown(notification));
        $('div.allnotifications').append(createNotification(notification));
    });

}

window.removeNotification=function($id){
    $.ajax({
            url: '/remove-notification/' + $id,
            type: 'GET',
            success: function(data) {

                    newsuccessFunction(data);

            }
        }

    );
};
window.acceptFriend=function($id){
    $.ajax({
            url: '/accept-friend/' + $id,
            type: 'GET',
            success: function(data) {
                newsuccessFunction(data);
            }
        }

    );
};
window.dennyFriend=function($id){
    $.ajax({
            url: '/denny-friend/' + $id,
        type: 'GET',
        success: function(data) {
            newsuccessFunction(data);
        }
        }

    );
};

window.acceptWishlist=function($id){
    $.ajax({
            url: '/accept-wishlist/' + $id,
        type: 'GET',
        success: function(data) {
            newsuccessFunction(data);
        }
        }

    );
};

window.dennyWishlist=function($id,$type){
    $.ajax({
            url: '/denny-wishlist/' + $id,
        type: 'GET',
        success: function(data) {
            newsuccessFunction(data);
        }
        }

    );
};
function createNotificationDropdown(n) {
    var $not='';
$not='<li class="notification" id="not_'+ n.id +'">'+ createNotification(n)+' </li>';
    return $not;
}

function createNotification(n) {
    var $not='';
    $not='<div class="media"> <div class="media-body">';
    $not+='<span class="glyphicon glyphicon-remove remove-notification" onclick="removeNotification('+ n.id +')" style="float: right;"></span>';
    if( n.type==="FRIEND_R" )
        $not+='<p class="notification-title"><b>'+n.sourceUserName  +'</b> vás požádal o přátelství <a href="#" onclick="acceptFriend('+ n.id +')" >Přijmout</a> <a href="#" onclick="dennyFriend('+ n.id +')">Odmítnout</a></p>';
    else if(  n.type==="FRIEND_A" )
        $not+='<p class="notification-title"><b>'+ n.sourceUserName  +'</b> přijmul vaši žádost o přátelství</p>';
    else if ( n.type==="FRIEND_D" )
        $not+='<p class="notification-title"><b>'+ n.sourceUserName  +'</b> odmítnul vaši žádost o přátelství </p>';
    else if ( n.type==="WISHLIST_R" )
        $not+='<p class="notification-title"><b>'+ n.sourceUserName  +'</b> vás pozval do nového nákupního seznamu <b> '+ n.wishlist.name + '</b> <a href="#" onclick="acceptWishlist('+ n.id +')">Přijmout</a> <a href="#" onclick="dennyWishlist('+ n.id +')">Odmítnout</a></p>';
    else if (n.type==="WISHLIST_A" )
        $not+='<p class="notification-title"><b>'+ n.sourceUserName  +'</b> přijmul vaše pozvání do nákupního seznamu <b>'+ n.wishlist.name +'</b></p>';
    else if ( n.type==="WISHLIST_D" )
        $not+='<p class="notification-title"><b>'+ n.sourceUserName  +'</b>  odmítnul vaše pozvání do nákupního seznamu <b>'+ n.wishlist.name +'</b></p>';
    else if ( n.type==="IWISHLIST_D")
        $not+='<p class="notification-title"><b>'+ n.sourceUserName  +'</b> vás odstranil z nákupního seznamu <b>'+ n.wishlistName +'</b></p>';
    else if ( n.type==="IWISHLIST_UD" )
        $not+='<p class="notification-title"><b>'+ n.sourceUserName  +'</b> opustil váš nákupní seznam <b>'+ n.wishlist.name +'</b></p>';
    else if ( n.type==="UFRIEND_R" )
        $not+='<p class="notification-title"><b>'+n.sourceUserName  +'</b>  si vás odstranil z přátel</p>';
    var d=new Date(Date.parse(n.created));
    var day=d.getDate();
    var month=d.getMonth()+1;
    var year=d.getFullYear();
    var hour=d.getHours();
    var minute=d.getMinutes();
    if (minute < 10){
        minute = "0" + minute;
    }
    $not+='<div class="notification-meta"> <small class="timestamp">'+hour+':'+minute+' '+day+'.'+month+'.'+year +'</small> </div> </div> </div> ';
return $not;
}