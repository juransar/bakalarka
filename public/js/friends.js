$('form :submit').attr("disabled", "disabled");


$('#friend_name').on('typeahead:selected', function(evt, item) {
    $('form :submit').attr("disabled", false);

});
$('#friend_name').typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    },
    {
        limit: 12,
        async: true,
        source: function (query, processSync, processAsync) {
            return $.ajax({
                url: "/ajaxfriends",
                type: 'GET',
                data: {query: query},
                dataType: 'json',
                success: function (json) {
                    if(json.indexOf(query)> -1) {
                        $('form :submit').attr("disabled", false);

                    }
                    else {
                        $('form :submit').attr("disabled", "disabled");

                    }
                    // in this example, json is simply an array of strings
                    return processAsync(json);
                }
            });
        },
    });

function warnMessage() {
    var result=confirm("Pokud si uživatele smažete z přátel, nebudete již moc editovat seznamy tohoto přítele, které vám nasdílel, a naopak. Přejete si pokračovat ?");
    if (result=== false) {
        event.preventDefault();

    }
}