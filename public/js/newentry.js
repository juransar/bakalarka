$(document).ready(function() {
    $('.emptyAisle').hide();

    function bindSortable() {
        $("#sort tbody").sortable({
            handle: '.handle',
            start: function (event, ui) {
                ui.item.startPos = ui.item.index() - ui.item.prevAll('.aisleName').length;
                $('.emptyAisle').show();
                $("#sort tbody").sortable('refresh');
            },
            stop: function (event, ui) {
                $('.emptyAisle').hide();
                var numAisles = ui.item.prevAll('.aisleName').length;

                $.ajax({
                    url: "/change-position",
                    type: 'GET',
                    data: {
                        id: ui.item.attr('id'),
                        oldPosition: ui.item.startPos,
                        newPosition: ui.item.index() - numAisles,
                        aisle: ui.item.prevAll('.aisleName:first').attr('id')
                    },
                    success: function (data) {

                        $('tbody.ui-sortable').empty();
                        if (!data.sorted)
                            updateEntries(data);
                        else {
                            var emptyAisles = [];
                            sum = 0;
                            $.each(JSON.parse(data.aisles), function (i, aisle) {
                                console.log(i + aisle);
                                if (!jQuery.isEmptyObject(aisle)) {
                                    sum += createAisle(i, aisle);
                                }
                                else emptyAisles.push(i);

                            });
                            for (aisle of emptyAisles) createEmptyAisle(aisle);
                            sum = sum || 0;
                            $('tbody.ui-sortable').append('<tr><td colspan="6" align="right"><b>Celková cena: </b>' + sum + '</td></tr>');

                        }
                        $('.emptyAisle').hide();
                        bindSortable();
                    }
                });
            }
        });
    }

    bindSortable();

    $(document).on('submit', 'form[name="entry"]', function (event) {
        event.preventDefault();
        $('#entry_Přidat').attr("disabled", true);
        var $form = $('form[name="entry"]');
        var data = {};
        var $entry = $('#entry_name');
        data[$entry.attr('item.name')] = $entry.val();
        data = new FormData($('form[name="entry"]')[0]);
        //data = $("form").serialize();
        $.ajax({
            url: $form.attr('action'),
            type: $form.attr('method'),
            processData: false,
            contentType: false,
            data: data,
            success: function (data) {
                if (data.result == 0) {
                    for (var key in data.data) {
                        $('<p id="error_' + key + '" style="color: red;">' + data.data[key] + '</p>').insertAfter($('form').find('[name*="' + key + '"]')[0]);
                    }
                }
                else {
                    $('tbody.ui-sortable').empty();
                    if (!data.sorted)
                        updateEntries(data);
                    else {
                        var emptyAisles = [];
                        sum = 0;
                        $.each(JSON.parse(data.aisles), function (i, aisle) {
                            console.log(i + aisle);
                            if (!jQuery.isEmptyObject(aisle)) {
                                sum += createAisle(i, aisle);
                            }
                            else emptyAisles.push(i);

                        });
                        for (aisle of emptyAisles) createEmptyAisle(aisle);
                        sum = sum || 0;
                        $('tbody.ui-sortable').append('<tr><td colspan="6" align="right"><b>Celková cena: </b>' + sum + '</td></tr>');

                    }
                    $('form')[0].reset();
                }
                $('#entry_Přidat').attr("disabled", false);
                $('.emptyAisle').hide();
                bindSortable();
            }
        });
    });
    $(document).on('change', '#entry_image', function () {
        if ($('#error_image').length) {
            $('#error_image').remove();
        }
    });

    $(document).on('change', '#entry_price', function () {
        if ($('#error_price').length) {
            $('#error_price').remove();
        }
    });
    $(document).on('input', '#entry_name', function () {

        $('#error_name').remove();

    });
    $(document).on('change', '.move-checkbox', function (event) {
        $id = event.target.id;
        $.ajax({
                url: '/entry/check/' + $id,
                type: 'GET',
                success: function () {
                    var tr = $('tr#' + $id);
                    if (tr.find("td[id=" + $id + "]").hasClass('checked'))
                        tr.find("td[id=" + $id + "]").removeClass('checked');
                    else tr.find("td[id=" + $id + "]").addClass('checked');
                    bindSortable();
                    $('.emptyAisle').hide();

                }
            }
        );
    });
    $(document).on('click', '.remove-entry', function (event) {
        $id = event.target.id;
        $.ajax({
                url: '/entry/remove/' + $id,
                type: 'POST',
                success: function () {
                    var tr = $('tr#' + $id);
                    if (tr.nextUntil('.aisleName').length === 0 && tr.prevUntil('.aisleName').length === 0) {
                        tr.prevAll('.aisleName:first').addClass('emptyAisle');
                    }
                    tr.remove();

                    bindSortable();
                    $('.emptyAisle').hide();
                }
            }
        );
    });


// When sport gets selected ...
    window.transferEntries = function ($id) {
        // ... retrieve the corresponding form.
        // Simulate form data, but only include the selected sport value.
        var $wishlist = $('#transfer_wishlists');
        var data = {};
        data[$wishlist.attr('name')] = $wishlist.val();
        // Submit data via AJAX to the form's action path.
        $.ajax({
            url: '/wishlist/transfer/' + $id,
            type: 'POST',
            data: data,
            success: function (html) {
                $('#transfer_entries').replaceWith(
                    $(html).find('#transfer_entries')
                );
                bindSortable();
            }
        });
    };

    $('#entry_name').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            limit: 12,
            async: true,
            source: function (query, processSync, processAsync) {
                return $.ajax({
                    url: "/ajax-items",
                    type: 'GET',
                    data: {query: query},
                    dataType: 'json',
                    success: function (json) {
                        return processAsync(json);
                    }
                });
            },
        });

    function updateEntries(data) {
        var sum = 0;
        $.each(JSON.parse(data.entries), function (i, entry) {
            sum += parseFloat(entry.price) || 0;
            $('tbody.ui-sortable').append(createRow(entry));

        });
        $('tbody.ui-sortable').append('<tr><td colspan="6" align="right"><b>Celková cena: </b>' + sum + '</td></tr>');
    }

    function createRow(entry, sortable) {
        if(sortable === undefined) sortable=true;
        var $tr = $('<tr/>', {id: entry.id, class: "checkbx"});
        var $td = $('<td/>', {id: entry.id});

        var $ch = $('<input/>', {class: "move-checkbox", type: "checkbox", name: "packersOff", id: entry.id});
        if (entry.checked) {
            $td.addClass("checked");
            $ch.prop("checked", true);
        }
        $td.append($ch);
        $tr.append($td);
        $td = $('<td/>');
        $td.append($('<label/>', {class: "strikethrough", text: entry.item.name}));
        $tr.append($td);
        $td = $('<td/>', {align: "center"});
        if (entry.amount) $td.text(entry.amount);
        $tr.append($td);
        $td = $('<td/>', {align: "center"});
        if (entry.price) $td.text(entry.price);
        $tr.append($td);
        $td = $('<td/>', {align: "center"});
        if (entry.image) {
            $a = $('<a/>', {href: "/uploads/images/" + entry.image, target: "_blank"});
            $a.append($('<span/>', {class: "glyphicon glyphicon-picture"}));
            $td.append($a);
        }
        if (entry.link) {
            $a = $('<a/>', {href: entry.link, target: "_blank"});
            $a.append($('<span/>', {class: "glyphicon glyphicon-link"}));
            $td.append($a);
        }
        $tr.append($td);
        $td = $('<td/>', {align: "center"});
        $td.append($("<span/>",{class: "glyphicon glyphicon-remove remove-entry",id:entry.id}));
        $tr.append($td);
        if(sortable) {
            console.log("adding handle");
            $tr.append($('<td/>').append($("<span/>",{class: "hidden-xs handle glyphicon glyphicon-resize-vertical"})));
        }
        else
            $tr.append($('<td/>'));
        return $tr;
    }

    function createAisle(aisle, entries) {
        sum = 0;

        $('tbody.ui-sortable').append('<tr class="aisleName" id="' + aisle + '"> ' +
            '<td colspan="6" align="left" style="color: #4d9887"><b>' + aisle + ' </b></td> </tr>');

        $.each(entries, function (i, entry) {
            sum += parseFloat(entry.price) || 0;
            $('tbody.ui-sortable').append(createRow(entry, entry.item.deletable));

        });
        return sum;
    }

    function createEmptyAisle(aisle) {

        $('tbody.ui-sortable').append('<tr class="aisleName hidden-xs emptyAisle" id="' + aisle + '"> ' +
            '<td colspan="6" align="left" style="color: #4d9887"><b>' + aisle + ' </b></td> </tr>');

    }

    $(document).ready(function () {
        console.log("ready!");
        if ($('#invitation_friends').is(':empty')) {
            $('#invitation_friends').append('<p>Zatím nemáte žádné uživatele v přátelích nebo jsou již všichni vaši přátelé pozvaní</p>');
        }
    });
});