$(document).ready(function() {

    function warnMessageDelete() {
        var result = confirm("Opravdu si přejete smazat profil ? Tato akce je nenávratná.");
        if (result === false) {
            event.preventDefault();

        }
    }

    $(document).on('change', '#notifications-toggle', function () {
        var checked=$(this).prop('checked');
        $.ajax({
                url: '/set-notifications/' ,
                type: 'GET',
                data: {notifications: checked ? 1:0},
                dataType: 'json',
            success: function(data) {


            }
            }

        );
    });

});