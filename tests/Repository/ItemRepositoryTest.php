<?php
/**
 * Created by PhpStorm.
 * User: sara
 * Date: 11.2.18
 * Time: 15:30
 */
namespace App\Tests\Repository;

use App\Entity\Item;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\Common\Persistence\ObjectManager;


class ItemRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    /** @var Item */
    protected $entity;
    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $this->entity=new Item();
        $this->entity->setName("testitem");

    }


    public function testFindByName(){
        $this->em->getRepository(Item::class)->save($this->entity);
        $this->assertEquals($this->entity->getId(),$this->em->getRepository(Item::class)->findByName("testitem")->getId());
        $this->em->getRepository(Item::class)->remove($this->entity);


    }


    public function testfindTypeahead(){

        $this->em->getRepository(Item::class)->save($this->entity);

        $this->assertNotContains($this->entity,$this->em->getRepository(Item::class)->findTypeahead("sti"));
        $this->assertEquals(1,count($this->em->getRepository(Item::class)->findTypeahead("pre")));
        $this->em->getRepository(Item::class)->remove($this->entity);


    }
    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null; // avoid memory leaks
    }
}