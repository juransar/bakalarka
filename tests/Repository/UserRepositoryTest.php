<?php
/**
 * Created by PhpStorm.
 * User: sara
 * Date: 11.2.18
 * Time: 15:30
 */
namespace App\Tests\Repository;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\Common\Persistence\ObjectManager;


class UserRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    /** @var User */
    protected $entity;
    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $this->entity=new User();
        $this->entity->setUsername("testuser");
        $this->entity->setMail("testmail");
        $this->entity->setPassword("password");

    }
    public function testCRUD(){

        $this->em->getRepository(User::class)->save($this->entity);
        $id=$this->entity->getId();
        $this->assertEquals($this->entity,$this->em->getRepository(User::class)->findById($id));
        $this->assertEquals(1,$this->count($this->em->getRepository(User::class)->findAllByClass()));
        $this->entity->setMail("changedmail");
        $this->em->getRepository(User::class)->save($this->entity);
        $this->assertEquals("changedmail",$this->em->getRepository(User::class)->findById($id)->getMail());

        $this->em->getRepository(User::class)->remove($this->entity);
        $this->assertNull($this->em->getRepository(User::class)->findById($id));

    }

public function testFindByUserName(){
    $this->em->getRepository(User::class)->save($this->entity);
    $this->assertEquals($this->entity->getId(),$this->em->getRepository(User::class)->findByUsername("testuser")->getId());
    $this->em->getRepository(User::class)->remove($this->entity);


}
    public function testfindByHash() {
        $this->em->getRepository(User::class)->save($this->entity);

        $this->assertEquals($this->entity->getId(),$this->em->getRepository(User::class)->findByHash($this->entity->getDeletehash())->getId());
        $this->em->getRepository(User::class)->remove($this->entity);

    }

    public function testfindTypeahead(){

        $this->em->getRepository(User::class)->save($this->entity);

        $this->assertContains($this->entity,$this->em->getRepository(User::class)->findTypeahead("stu"));
        $this->em->getRepository(User::class)->remove($this->entity);


    }
    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null; // avoid memory leaks
    }
}