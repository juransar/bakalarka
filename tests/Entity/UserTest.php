<?php
/**
 * Created by PhpStorm.
 * User: sara
 * Date: 10.2.18
 * Time: 17:17
 */
namespace App\Tests\Entity;

use App\Entity\User;
use App\Entity\Wishlist;
use PHPUnit\Framework\TestCase;
class UserTest extends TestCase
{
    /** @var User */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new User();
    }
    public function testId()
    {
        $id = rand(0, 100);
        $this->entity->setId($id);
        $this->assertEquals($id, $this->entity->getId());
    }

    public function testUsername()
    {
        $username = "user".rand(0, 100);
        $this->entity->setUsername($username);
        $this->assertEquals($username, $this->entity->getUsername());
    }

    public function testPassword()
    {
        $password = "passwd".rand(0, 100);
        $this->entity->setPassword($password);
        $this->assertEquals($password, $this->entity->getPassword());
    }

    public function testMail()
    {
        $mail = "mail".rand(0, 100);
        $this->entity->setMail($mail);
        $this->assertEquals($mail, $this->entity->getMail());
    }

    public function testAddWishlist(){
        $this->entity->setId(1);
        $this->entity->setUsername("testuser");
        $this->entity->setMail("testmail");
        $this->entity->setPassword("password");

        $wishlist=new Wishlist();
        $wishlist->setName("testwishlist");

        $this->entity->addWishlist($wishlist);
        $user=$wishlist->getOwner();
        $this->assertEquals( $this->entity,$user );
        $this->assertContains($wishlist,$this->entity->getWishlists());
        return [$this->entity,$wishlist];

    }
    /**
     * @depends testAddWishlist
     */
    public function testRemoveWishlist($data){

        list($user, $wishlist) = $data;
        $user->removeWishlist($wishlist);
        $users=$wishlist->getUsers();
        $this->assertNotContains( $this->entity,$users );

        $this->assertNotContains( $wishlist,$this->entity->getWishlists() );

    }

    public function testAddInvitedWishlist(){
        $this->entity->setUsername("testuser");
        $this->entity->setMail("testmail");
        $this->entity->setPassword("password");

        $wishlist=new Wishlist();
        $wishlist->setName("testwishlist");

        $this->entity->addInvitedWishlist($wishlist);
        $users=$wishlist->getUsers();
        $this->assertContains( $this->entity,$users );
        $this->assertContains($wishlist,$this->entity->getInvitedWishlists());
        return [$this->entity,$wishlist];

    }

    /**
     * @depends testAddInvitedWishlist
     */
    public function testRemoveInvitedWishlist($data){

        list($user, $wishlist) = $data;
        $user->removeInvitedWishlist($wishlist);
        $users=$wishlist->getUsers();
        $this->assertNotContains( $this->entity,$users );

        $this->assertNotContains( $wishlist,$this->entity->getInvitedWishlists() );

    }

    public function testAddFriend(){
        $this->entity->setUsername("testuser");
        $this->entity->setMail("testmail");
        $this->entity->setPassword("password");

        $friend=new User();
        $friend->setUsername("testfriend");

        $this->entity->addFriend($friend);
        $this->assertContains($friend,$this->entity->getFriends());
        return [$this->entity,$friend];

    }
    /**
     * @depends testAddFriend
     */
    public function testRemoveFriend($data){

        list($user, $friend) = $data;
        $user->removeFriend($friend);
        $this->assertNotContains( $friend,$this->entity->getFriends());

    }


}