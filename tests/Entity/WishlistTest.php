<?php
/**
 * Created by PhpStorm.
 * User: sara
 * Date: 11.2.18
 * Time: 2:28
 */

namespace App\Tests\Entity;

use App\Entity\Entry;
use App\Entity\User;
use App\Entity\Wishlist;
use PHPUnit\Framework\TestCase;

class WishlistTest extends TestCase
{
    /** @var Wishlist */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new Wishlist();
    }
    public function testId()
    {
        $id = rand(0, 100);
        $this->entity->setId($id);
        $this->assertEquals($id, $this->entity->getId());
    }
    public function testName()
    {
        $name= "wishlist".rand(0, 100);
        $this->entity->setName($name);
        $this->assertEquals($name, $this->entity->getName());
    }

    public function testModified()
    {
        $time = new \DateTime();
        $this->entity->setModified($time);
        $this->assertEquals($time, $this->entity->getModified());
    }

    public function testAddOwner(){

        $user=new User();
        $user->setUsername("testuser");

        $this->entity->setOwner($user);
        $wishlists=$user->getWishlists();
        $this->assertContains( $this->entity,$wishlists );
        $this->assertEquals($user,$this->entity->getOwner());
        return [$this->entity,$user];

    }
    /**
     * @depends testAddUser
     */
    public function testRemoveUser($data){

        list($wishlist, $user) = $data;
        $wishlist->removeUser($user);
        $wishlists=$user->getWishlists();
        $this->assertNotContains($this->entity,$wishlists );

        $this->assertNotContains( $user,$this->entity->getUsers() );

    }

    public function testAddUser(){

        $user=new User();
        $user->setUsername("testuser");

        $this->entity->addUser($user);
        $wishlists=$user->getInvitedWishlists();
        $this->assertContains( $this->entity,$wishlists );
        $this->assertContains($user,$this->entity->getUsers());
        return [$this->entity,$user];

    }
    /**
     * @depends testAddInvitedUser
     */
    public function testRemoveInvitedUser($data){

        list($wishlist, $user) = $data;
        $wishlist->removeInvitedUser($user);
        $wishlists=$user->getInvitedWishlists();
        $this->assertNotContains($this->entity,$wishlists );

        $this->assertNotContains( $user,$this->entity->getInvitedUsers() );

    }

    public function testAddEntry(){

        $entry=new Entry();
        $entry->setId(rand(0,100));
        $this->entity->addEntry($entry);
        $this->assertEquals( $this->entity,$entry->getWishlist());
        $this->assertContains($entry,$this->entity->getEntries());
        return [$this->entity,$entry];

    }
    /**
     * @depends testAddEntry
     */
    public function testRemoveEntry($data){

        list($wishlist, $entry) = $data;
        $wishlist->removeEntry($entry);

        $this->assertNotContains( $entry,$this->entity->getEntries() );

    }
}