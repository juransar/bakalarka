<?php
/**
 * Created by PhpStorm.
 * User: sara
 * Date: 11.2.18
 * Time: 13:28
 */

namespace App\Tests\Entity;

use App\Entity\Item;
use PHPUnit\Framework\TestCase;
class ItemTest extends TestCase
{
    /** @var Item */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new Item();
    }
    public function testId()
    {
        $id = rand(0, 100);
        $this->entity->setId($id);
        $this->assertEquals($id, $this->entity->getId());
    }

    public function testName()
    {
        $name= "wishlist".rand(0, 100);
        $this->entity->setName($name);
        $this->assertEquals($name, $this->entity->getName());
    }
}