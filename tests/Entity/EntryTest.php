<?php
/**
 * Created by PhpStorm.
 * User: sara
 * Date: 11.2.18
 * Time: 13:20
 */

namespace App\Tests\Entity;

use App\Entity\Entry;
use App\Entity\Item;
use App\Entity\Wishlist;
use PHPUnit\Framework\TestCase;

class EntryTest extends TestCase
{

    /** @var Entry */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new Entry();
    }
    public function testId()
    {
        $id = rand(0, 100);
        $this->entity->setId($id);
        $this->assertEquals($id, $this->entity->getId());
    }

    public function testChecked()
    {
        $checked=true;
        $this->entity->setChecked($checked);
        $this->assertEquals($checked, $this->entity->getChecked());
    }

    public function testLink()
    {
        $link = "link".rand(0, 100);
        $this->entity->setLink($link);
        $this->assertEquals($link, $this->entity->getLink());
    }

    public function testAmount()
    {
        $amount = "am ".rand(0, 100);
        $this->entity->setAmount($amount);
        $this->assertEquals($amount, $this->entity->getAmount());
    }
    public function testPrice()
    {
        $price = rand(0, 100)+1.2;
        $this->entity->setPrice($price);
        $this->assertEquals($price, $this->entity->getPrice());
    }

    public function testImage()
    {
        $image = "image".rand(0, 100);
        $this->entity->setImage($image);
        $this->assertEquals($image, $this->entity->getImage());
    }

    public function testItem(){
        $item= new Item();
        $item->setId(rand(0,100));
        $this->entity->setItem($item);
        $this->assertEquals($item, $this->entity->getItem());

    }

    public function testWishlist(){
        $wishlist= new Wishlist();
        $wishlist->setId(rand(0,100));
        $this->entity->setWishlist($wishlist);
        $this->assertEquals($wishlist, $this->entity->getWishlist());
        $this->assertContains($this->entity, $wishlist->getEntries());

    }
    public function testPosition(){
        $wishlist= new Wishlist();
        $wishlist->setId(rand(0,100));
        $this->entity->setWishlist($wishlist);
        $entry2=new Entry();
        $entry2->setWishlist($wishlist);
        $this->assertEquals(0, $this->entity->getPositionInList());
        $this->assertEquals(1, $entry2->getPositionInList());
        $this->assertContains($this->entity, $wishlist->getEntries());

    }
}