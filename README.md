# Sdílené nákupní seznamy

## O aplikaci
 Tato aplikace je webová aplikace k vytváření a sdílení nákupních seznamů. Jedná se o implementační část bakalářské práce studentky Sáry Jurankové z ČVUT FIT. Nasazenou aplikaci najdete na http://159.89.11.237


## Instalace
Pro správnou instalaci aplikace je potřeba mít na serveru nainstalováno PHP 7.2 a MySQL, dále je potřeba mít pro aplikaci e-mailovou adresu, ze které budou posílány uživatelům informační e-maily.

Je nutné provést minimálně tyto kroky:

1. nahrajte aplikace na produkční server
2. nainstalujte závislosti pomocí Composeru příkazem:
``` composer install --no-dev --optimize-autoloader```
3. v MySQL vytvořte novou databázi
4. v souboru .env je potřeba nakonfigurovat připojení k této databázi například takto: ```DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name```
5. podobně v .env nakonfigurujte e-mailovou adresu, pro gmail to bude například: ```MAILER_URL=gmail://user:password@localhost```
6. v .env upravte hodnotu proměnné ```APP_ENV``` na ```APP_ENV=prod```
7. vytvořte databázi pomocí příkazu ```php bin/console doctrine:database:create```
8. vymažte cache pomocí ```php bin/console cache:clear && php bin/console cache:warmup --env=prod```


Dále je potřeba správně nastavit přístupová práva ke složkám a upravit konfigurační soubor serveru.
Tyto kroky jsou specifické pro vaše konkrétní prostředí (např. Apache/Nginx apod.), pro bližší informace k instalaci 
tedy navštivte:

- https://symfony.com/doc/current/deployment.html
- https://symfony.com/doc/current/setup/web_server_configuration.html
- https://www.digitalocean.com/community/tutorials/how-to-deploy-a-symfony-application-to-production-on-ubuntu-14-04



