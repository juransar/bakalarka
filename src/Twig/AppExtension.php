<?php
/**
 * Created by PhpStorm.
 * User: sara
 * Date: 5.4.18
 * Time: 0:46
 */

namespace App\Twig;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;


class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return array(
            new TwigFilter('aisle', array($this, 'checkAisle')),
            new TwigFilter('esort', array($this, 'sortEntries')),
        );
    }

    public function checkAisle($arr, $aisle)
    {
        foreach ($arr as $item){
            if ($item->getItem()->getAisle()==$aisle) return true;
        }

        return false;
    }

    public function sortEntries($items)
    {
       /* usort($items->getValues(), function ($item1, $item2) {
            if ($item1->getPositionInList() == $item2->getPositionInList()) return 0;
            return $item1->getPositionInList() < $item2->getPositionInList() ? -1 : 1;
        });
*/
        $array = $items->getValues();
        usort($array, function($a, $b){
            return ($a->getPositionInList()< $b->getPositionInList()) ? -1 : 1 ;
        });


        return $array;
    }
}