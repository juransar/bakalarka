<?php
/**
 * Created by PhpStorm.
 * User: sara
 * Date: 8.3.18
 * Time: 18:02
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Functionality\NotificationFunctionality;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Entity\Notification;
use App\Entity\User;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\JsonResponse;

class NotificationController extends Controller
{

    protected function serializeNotifications($notifications){
        $notifications=$notifications->getValues();
        usort($notifications, function($a, $b){
            return ($a->getCreated()< $b->getCreated()) ? 1 : -1 ;

        });
        $callback = function ($dateTime) {
            return $dateTime instanceof \DateTime
                ? $dateTime->format(\DateTime::ISO8601)
                : '';
        };
        $encoders = array( new JsonEncoder());
        $normalizer=new ObjectNormalizer();
        $normalizer->setIgnoredAttributes(array("sourceUser","targetUser"));

        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $normalizer->setCallbacks(array('created' => $callback));

        $normalizers = array($normalizer);

        $serializer = new Serializer($normalizers, $encoders);
        return $serializer->serialize($notifications,'json');
    }
    /**
     * @Route("/accept-wishlist/{id}", name="accept_wishlist")
     */
    public function acceptInvitationAction(Request $request,$id, NotificationFunctionality $notificationFunctionality)
    {
        $notification = $this->getDoctrine()->getRepository(Notification::class)->findById($id);
        $this->denyAccessUnlessGranted('edit', $notification);
        $notificationFunctionality->acceptWishlistRequest($notification);

        return new JsonResponse(['not_count'=>count($this->getUser()->getNotifications()),"notifications"=>$this->serializeNotifications($this->getUser()->getNotifications())]);

    }

    /**
     * @Route("/denny-wishlist/{id}", name="denny_wishlist")
     */
    public function dennyInvitationAction(Request $request,$id, NotificationFunctionality $notificationFunctionality)
    {
        $notification = $this->getDoctrine()->getRepository(Notification::class)->findById($id);
        $this->denyAccessUnlessGranted('edit', $notification);

        $notificationFunctionality->dennyWishlistRequest($notification);
        return new JsonResponse(['not_count'=>count($this->getUser()->getNotifications()),"notifications"=>$this->serializeNotifications($this->getUser()->getNotifications())]);

    }

    /**
     * @Route("/accept-friend/{id}", name="accept_friend")
     */
    public function addFriendAction(Request $request,$id, NotificationFunctionality $notificationFunctionality)
    {
        $notification = $this->getDoctrine()->getRepository(Notification::class)->findById($id);
        $this->denyAccessUnlessGranted('edit', $notification);

        $notificationFunctionality->acceptFriendRequest($notification);
        return new JsonResponse(['not_count'=>count($this->getUser()->getNotifications()),"notifications"=>$this->serializeNotifications($this->getUser()->getNotifications())]);

    }

    /**
     * @Route("/denny-friend/{id}", name="denny_friend")
     */
    public function dennyFriendAction(Request $request,$id, NotificationFunctionality $notificationFunctionality)
    {
        $notification = $this->getDoctrine()->getRepository(Notification::class)->findById($id);
        $this->denyAccessUnlessGranted('edit', $notification);

        $notificationFunctionality->dennyFriendRequest($notification);
        return new JsonResponse(['not_count'=>count($this->getUser()->getNotifications()),"notifications"=>$this->serializeNotifications($this->getUser()->getNotifications())]);

    }

    /**
     * @Route("/remove-notification/{id}", name="remove_notification")
     */
    public function removeNotificationAction(Request $request,$id, NotificationFunctionality $notificationFunctionality)
    {
        $notification = $this->getDoctrine()->getRepository(Notification::class)->findById($id);
        $this->denyAccessUnlessGranted('edit', $notification);

        $notificationFunctionality->removeNotification($notification);

        return new JsonResponse(['not_count'=>count($this->getUser()->getNotifications()),"notifications"=>$this->serializeNotifications($this->getUser()->getNotifications())]);
    }

    /**
     * @Route("/all-notifications/", name="all_notifications")
     */
    public function allNotificationsAction(Request $request)
    {
        return $this->render('Notification/allnotifications.html.twig');
    }

}