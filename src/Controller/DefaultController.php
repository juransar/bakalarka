<?php
/**
 * Created by PhpStorm.
 * User: sara
 * Date: 8.2.18
 * Time: 12:32
 */
namespace App\Controller;
use App\Entity\User;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\LoginForm;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function index( AuthenticationUtils $authUtils)
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('wishlists');
        }
        $user = new User();
        $form_signup = $this->createForm(UserType::class, $user,['action'=>$this->generateUrl('signup')]);
        $error = $authUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authUtils->getLastUsername();
        $form = $this->createForm(LoginForm::class, [
            '_username' => $lastUsername,
        ]);
        return $this->render(
            'index.html.twig',['form_signup'=>$form_signup->createView(),'form' => $form->createView(),
                'error' => $error,]);
    }
}