<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\EmailType;
use App\Form\FriendType;
use App\Form\UserType;
use App\Functionality\NotificationFunctionality;
use App\Functionality\UserFunctionality;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class UserController extends Controller
{
public function sendRegistrationEmail(User $user,\Swift_Mailer $mailer){

    $message = (new \Swift_Message('Registrace v aplikaci Sdílené seznamy'))
        ->setFrom('shared.lists.cvut@gmail.com')
        ->setTo($user->getMail())
        ->setBody(
            $this->renderView(
            // templates/emails/registration.html.twig
                'Email/emailregistration.html.twig',
                array('mail' => $user->getMail(),'link'=> $this->generateUrl('email-delete', array('hash' => $user->getDeletehash()), UrlGeneratorInterface::ABSOLUTE_URL))
            ),
            'text/html'
        );
    $mailer->send($message);
}
public function sendFriendRequestEmail( User $friend,\Swift_Mailer $mailer ){
    $message = (new \Swift_Message('Žádost o přátelství v aplikaci Sdílené seznamy'))
        ->setFrom('shared.lists.cvut@gmail.com')
        ->setTo($friend->getMail())
        ->setBody(
            $this->renderView(
            // templates/emails/registration.html.twig
                'Email/friendrequest.html.twig',
                array('friend' => $friend,'link'=> $this->generateUrl('email-delete', array('hash' => $friend->getDeletehash()), UrlGeneratorInterface::ABSOLUTE_URL))
            ),
            'text/html'
        );
    $mailer->send($message);
}
    /**
     * @Route("/signup", name="signup")
     */
    public function signUpAction(Request $request, UserPasswordEncoderInterface $passwordEncoder,\Swift_Mailer $mailer)
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('wishlists');
        }
        $user = new User();

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $this->getDoctrine()->getRepository(User::class)->save($user);
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());

            $user->setPassword($password);
            $this->getDoctrine()->getRepository(User::class)->save($user);
            $this->sendRegistrationEmail($user,$mailer);

            $this->addFlash(
                'notice',
                'Registrace proběhla úspěšně'
            );


            return $this->redirectToRoute('login');
        }
        return $this->render(
            'User/signup.html.twig',
            array('form_user' => $form->createView())
        );

    }

    /**
     * @Route("/friends", name="friends")
     */
    public function friendsAction(Request $request, NotificationFunctionality $notificationFunctionality = null,\Swift_Mailer $mailer )
    {
        $form = $this->createForm(FriendType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $username = $form->get('name')->getData();
            $source = $this->getUser();
            $user = $this->getDoctrine()->getRepository(User::class)->findByUsername($username);
            $notificationFunctionality->makeFriendRequest($source, $user);
            if($user->isEmailnotifications()) $this->sendFriendRequestEmail($user,$mailer);
            $this->addFlash(
                'notice',
                "Uživateli " . $username . " byla odeslána žádost o přátelství"
            );

            unset($form);
            $form = $this->createForm(FriendType::class);
        }
        return $this->render(
            'User/friends.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @Route("/removefriend/{id}", name="remove_friend")
     */
    public function removeFriendAction(Request $request, $id,UserFunctionality $userFunctionality = null)
    {
        $target = $this->getDoctrine()->getRepository(User::class)->findById($id);
        $source=$this->getUser();
        $this->denyAccessUnlessGranted('remove', $target);

        $userFunctionality->removeFriend($source,$target);
        return $this->redirectToRoute('friends');


    }

    /**
     * @Route("/ajaxfriends", name="ajaxfriends")
     */
    public function indexAction(Request $request)
    {
        $matches = $this->getDoctrine()->getRepository(User::class)->findTypeahead($request->get('query'));
         $users= array_diff(array_map(create_function('$o', 'return $o->getusername();'), $matches),[$this->getUser()->getUsername()]);

        if ($request->get('query')) {
            return new JsonResponse($users);
        }

        return $this->render('index.html.twig');
    }
    /**
     * @Route("/verify-delete/{hash}", name="email-delete")
     */
    public function unverifiedAction(Request $request,$hash,UserFunctionality $userFunctionality = null)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findByHash($hash);
        if($user==null){
            $this->addFlash(
                'error',
                "Účet, který se pokoušíte smazat, již neexistuje."
            );
            return $this->redirectToRoute('logout');
        }
        $this->get('security.token_storage')->setToken(null);
        $request->getSession()->invalidate();
        $userFunctionality->removeUser($user);
        $this->addFlash(
            'notice',
            "Účet byl úspěšně smazán"
        );
        return $this->redirectToRoute('logout');
    }

    /**
     * @Route("/edit-user/", name="edit_user")
     */
    public function editUserAction(Request $request,\Swift_Mailer $mailer)
    {
        $user=$this->getUser();
        $form = $this->createForm(EmailType::class,$user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getRepository(User::class)->save($user);
            $this->sendRegistrationEmail($user,$mailer);

            $this->addFlash(
                'notice',
                'Email byl změněn'
            );


        }
        return $this->render('User/user.html.twig',
            array('form' => $form->createView()));
    }

    /**
     * @Route("/delete-user/", name="delete_user")
     */
    public function deleteUserAction(Request $request,UserFunctionality $userFunctionality = null)
    {

        $userFunctionality->removeUser($this->getUser());
        $this->get('security.token_storage')->setToken(null);
        $request->getSession()->invalidate();
        $this->addFlash(
            'notice',
            "Účet byl úspěšně smazán"
        );
        return $this->redirectToRoute('logout');
    }
    /**
     * @Route("/set-notifications/", name="set_notifications")
     */
    public function setNotificationsAction(Request $request)
    {
        $this->getUser()->setEmailnotifications($request->get('notifications'));
        $this->getDoctrine()->getRepository(User::class)->save($this->getUser());
        return new JsonResponse();
    }
}
