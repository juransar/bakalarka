<?php
/**
 * Created by PhpStorm.
 * User: sara
 * Date: 15.2.18
 * Time: 23:36
 */

namespace App\Controller;

use App\Entity\Entry;
use App\Entity\Item;
use App\Entity\User;
use App\Entity\Wishlist;
use App\Form\EntryType;
use App\Form\InvitationType;
use App\Form\TransferType;
use App\Form\WishlistType;
use App\Functionality\EntryFunctionality;
use App\Functionality\NotificationFunctionality;
use App\Functionality\WishlistsFunctionality;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


class WishlistController extends Controller
{
    /**
     * @Route("/wishlists", name="wishlists")
     */
    public function wishlistsAction(Request $request, WishlistsFunctionality $wishlistsFunctionality = null)
    {
        $user = $this->getUser();
        $wishlist = new Wishlist();
        $form = $this->createForm(WishlistType::class, $wishlist);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $wishlistsFunctionality->makeWishlist($user, $wishlist);
            return $this->redirectToRoute('wishlists');
        }
        return $this->render(
            'Wishlist/wishlists.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @Route("/wishlists/invited", name="wishlists_invited")
     */
    public function invitedWishlistsAction(Request $request, WishlistsFunctionality $wishlistsFunctionality = null)
    {
        return $this->render(
            'Wishlist/invitedwishlists.html.twig'
        );
    }

    /**
     * @Route("/wishlists/{id}", name="wishlist", requirements={"id"="\d+"})
     */
    public function editWishlistAction(Request $request, $id, \Swift_Mailer $mailer, NotificationFunctionality $notificationFunctionality = null, EntryFunctionality $entryFunctionality = null)
    {
        $entry = new Entry();
        $wishlist = $this->getDoctrine()->getRepository(Wishlist::class)->findById($id);
        $this->denyAccessUnlessGranted('view', $wishlist);
        $form = $this->createForm(EntryType::class, $entry, ['action' => $this->generateUrl('entry_add', ['id' => $wishlist->getId()])]);
        $form2 = $this->buildTransferForm($wishlist);
        $form3 = $this->createForm(InvitationType::class, null, ['wishlist' => $wishlist]);
        $form3->handleRequest($request);
        if ($form3->isSubmitted() && $form3->isValid()) {

            foreach ($form3->get('friends')->getData() as $friend) $this->sendWishlistRequestEmail($friend, $wishlist, $mailer);
            $notificationFunctionality->makeInvitationRequest($this->getUser(), $form3->get('friends')->getData(), $wishlist);
            $this->addFlash(
                'notice',
                "Vašim přátelům byla odeslaná pozvánka do seznamu"
            );
        }
        return $this->render(
            'Wishlist/newentry.html.twig',
            array("wishlist" => $wishlist, 'form' => $form->createView(), 'form2' => $form2->createView(), 'form3' => $form3->createView(), 'entries' => $wishlist->getEntries(), 'aisles' => $this->getDoctrine()->getRepository(Item::class)->findAisles(), 'aisle')

        );
    }

    public function buildTransferForm($wishlist)
    {
        $wishlists = $this->getUser()->getWishlists();
        $invitedWishlists = $this->getUser()->getInvitedWishlists();
        $form = $this->createForm(TransferType::class, $wishlist, ['wishlists' => array_udiff(array_merge($wishlists->getValues(), $invitedWishlists->getValues()), [$wishlist], function ($obj_a, $obj_b) {
            return $obj_a->getId() - $obj_b->getId();
        }), 'action' => $this->generateUrl('wishlist_transfer', ['id' => $wishlist->getId()])]);
        return $form;
    }

    public function sendWishlistRequestEmail(User $friend, Wishlist $wishlist, \Swift_Mailer $mailer)
    {
        $message = (new \Swift_Message('Pozvání do nákupního seznamu v aplikaci Sdílené seznamy'))
            ->setFrom('shared.lists.cvut@gmail.com')
            ->setTo($friend->getMail())
            ->setBody(
                $this->renderView(
                // templates/emails/registration.html.twig
                    'Email/wishlistrequest.html.twig',
                    array('wishlist' => $wishlist, 'friend' => $friend, 'link' => $this->generateUrl('email-delete', array('hash' => $friend->getDeletehash()), UrlGeneratorInterface::ABSOLUTE_URL))
                ),
                'text/html'
            );
        $mailer->send($message);
    }

    /**
     * @Route("/wishlist/transfer/{id}", name="wishlist_transfer", requirements={"id"="\d+"})
     */
    public function transferToWishlistAction(Request $request, $id, EntryFunctionality $entryFunctionality = null)
    {
        $wishlist = $this->getDoctrine()->getRepository(Wishlist::class)->findById($id);

        $form = $this->buildTransferForm($wishlist);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entryFunctionality->transferEntries($form->get('entries')->getData(), $wishlist);
            return $this->redirectToRoute('wishlist', ['id' => $id]);
        }
        return $this->render(
            'Wishlist/transferform.html.twig',
            array('id' => $id, 'form' => $form->createView())
        );
    }

    /**
     * @Route("/wishlist/add-entry/{id}", name="entry_add", requirements={"id"="\d+"})
     */
    public function addToWishlistAction(Request $request, $id, EntryFunctionality $entryFunctionality = null)
    {
        $wishlist = $this->getDoctrine()->getRepository(Wishlist::class)->findById($id);
        $entry = new Entry();
        $form = $this->createForm(EntryType::class, $entry, ['action' => $this->generateUrl('entry_add', ['id' => $wishlist->getId()])]);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $entryFunctionality->saveEntry($entry, $name = $form->get('name')->getData(), $file = $form->get('image')->getData(), $wishlist);
                if ($entry->getWishlist()->isSorted()) {
                    return new JsonResponse(['result' => 1, 'sorted' => 1, 'aisles' => $this->serializeSortedEntries($wishlist->getEntries(), $this->getDoctrine()->getRepository(Item::class)->findAisles())]);
                } else {
                    return new JsonResponse(['result' => 1, 'sorted' => 0, 'entries' => $this->serializeEntries($wishlist->getEntries())]);

                }
            } else
                return new JsonResponse(['result' => 0, 'data' => $this->getErrorMessages($form)]);
        }
        return new Response();
    }

    protected function serializeSortedEntries($entries, $aisles)
    {
        $aisles = array_fill_keys($aisles, []);

        foreach ($entries as $entry)
            $aisles[$entry->getItem()->getAisle()][] = $entry;
        foreach ($aisles as $aisle)
            usort($aisle, function ($a, $b) {
                return ($a->getPositionInList() < $b->getPositionInList()) ? -1 : 1;
            });


        $encoders = array(new JsonEncoder());
        $normalizer = new ObjectNormalizer();
        $normalizer->setIgnoredAttributes(array("wishlist"));

        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $normalizers = array($normalizer);

        $serializer = new Serializer($normalizers, $encoders);
        return $serializer->serialize($aisles, 'json');
    }

    protected function serializeEntries($entries)
    {
        $entries = $entries->getValues();
        usort($entries, function ($a, $b) {
            return ($a->getPositionInList() < $b->getPositionInList()) ? -1 : 1;
        });
        $encoders = array(new JsonEncoder());
        $normalizer = new ObjectNormalizer();
        $normalizer->setIgnoredAttributes(array("wishlist"));

        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $normalizers = array($normalizer);

        $serializer = new Serializer($normalizers, $encoders);
        return $serializer->serialize($entries, 'json');
    }

    protected function getErrorMessages(FormInterface $form)
    {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }

    /**
     * @Route("/wishlists/delete/{id}", name="wishlist_delete", requirements={"id"="\d+"})
     */
    public function deleteWishlistAction($id, WishlistsFunctionality $wishlistsFunctionality = null)
    {

        $wishlist = $this->getDoctrine()->getRepository(Wishlist::class)->findById($id);
        $this->denyAccessUnlessGranted('delete', $wishlist);
        $wishlistsFunctionality->removeWishlist($wishlist);
        return $this->redirectToRoute('wishlists');


    }

    /**
     * @Route("/wishlists/sort/{id}", name="wishlist_sort", requirements={"id"="\d+"})
     */
    public function sortWishlistAction($id, WishlistsFunctionality $wishlistsFunctionality = null)
    {

        $wishlist = $this->getDoctrine()->getRepository(Wishlist::class)->findById($id);
        $this->denyAccessUnlessGranted('view', $wishlist);
        $wishlist->setSorted(!$wishlist->isSorted());
        $this->getDoctrine()->getRepository(Wishlist::class)->save($wishlist);
        return $this->redirectToRoute('wishlist', ['id' => $id]);


    }

    /**
     * @Route("/entry/check/{id}", name="entry_change", requirements={"id"="\d+"})
     */
    public function changeEntryAction($id, EntryFunctionality $entryFunctionality = null)
    {
        $entry = $this->getDoctrine()->getRepository(Entry::class)->findById($id);
        $this->denyAccessUnlessGranted('view', $entry->getWishlist());
        $entryFunctionality->changeEntry($entry);
        return new Response();

    }

    /**
     * @Route("/entry/remove/{id}", name="entry_remove", requirements={"id"="\d+"})
     */
    public function removeEntryAction($id, EntryFunctionality $entryFunctionality = null)
    {

        $entry = $this->getDoctrine()->getRepository(Entry::class)->findById($id);
        $this->denyAccessUnlessGranted('view', $entry->getWishlist());

        $entryFunctionality->removeEntry($entry);
        return new Response();
    }

    /**
     * @Route("/wishlist/remove-user/{id}/{u_id}", name="wishlist_remove_user", requirements={"id"="\d+","u_id"="\d+"})
     */
    public function removeUserAction($id, $u_id, WishlistsFunctionality $wishlistsFunctionality = null)
    {

        $wishlist = $this->getDoctrine()->getRepository(Wishlist::class)->findById($id);
        $this->denyAccessUnlessGranted('delete', $wishlist);
        $user = $this->getDoctrine()->getRepository(User::class)->findById($u_id);
        $wishlistsFunctionality->removeUserFromWishlist($user, $wishlist);
        return $this->redirectToRoute('wishlist', array('id' => $id));

    }

    /**
     * @Route("/wishlists/invited/delete/{id}", name="wishlist_delete_invited")
     */
    public function deleteInvitedWishlistAction(Request $request, $id, WishlistsFunctionality $wishlistsFunctionality = null)
    {
        $wishlist = $this->getDoctrine()->getRepository(Wishlist::class)->findById($id);
        $this->denyAccessUnlessGranted('view', $wishlist);
        $wishlistsFunctionality->removeIvitedWishlist($this->getUser(), $wishlist);
        return $this->redirectToRoute('wishlists_invited');


    }

    /**
     * @Route("/wishlists/export/{id}", name="wishlist_export", requirements={"id"="\d+"})
     */
    public function exportWishlistAction($id, \Swift_Mailer $mailer, WishlistsFunctionality $wishlistsFunctionality = null)
    {

        $wishlist = $this->getDoctrine()->getRepository(Wishlist::class)->findById($id);
        $this->denyAccessUnlessGranted('view', $wishlist);
        $this->sendExportEmail($this->getUser(), $wishlist, $mailer);
        $this->addFlash(
            'notice',
            "Seznam byl odeslán na vaši e-mailovou adresu"
        );
        return $this->redirectToRoute('wishlist', ['id' => $id]);


    }

    public function sendExportEmail(User $user, Wishlist $wishlist, \Swift_Mailer $mailer)
    {
        $template = 'Email/wishlistexport.html.twig';
        $options = ['wishlist' => $wishlist];
        if ($wishlist->isSorted()) {
            $template = 'Email/sortedwishlistexport.html.twig';
            $options += ['aisles' => $this->getDoctrine()->getRepository(Item::class)->findAisles()];
        }

        $message = (new \Swift_Message('Export nákupního seznamu ' . $wishlist->getName()))
            ->setFrom('shared.lists.cvut@gmail.com')
            ->setTo($user->getMail())
            ->setBody(
                $this->renderView(
                // templates/emails/registration.html.twig
                    $template,
                    $options
                ),
                'text/html'
            );
        $mailer->send($message);
    }

    /**
     * @Route("/ajax-items", name="ajax_items")
     */
    public function itemsTypeaheadAction(Request $request)
    {
        $matches = $this->getDoctrine()->getRepository(Item::class)->findTypeahead($request->get('query'));

        $items = array_map(create_function('$o', 'return $o->getname();'), $matches);
        foreach (array_merge($this->getUser()->getWishlists()->getValues(), $this->getUser()->getInvitedWishlists()->getValues()) as $wishlist) {
            foreach ($wishlist->getEntries() as $entry) {
                if (strpos($entry->getItem()->getName(), $request->get('query')) !== FALSE) $items[] = $entry->getItem()->getName();
            }
        }
        if ($request->get('query')) {
            return new JsonResponse(array_unique($items));
        }

        return $this->render('index.html.twig');
    }

    /**
     * @Route("/change-position", name="ajax_position")
     */
    public function changeEntryPosition(Request $request)
    {
        $id = $request->get('id');
        $oldPos = $request->get('oldPosition');
        $newPos = $request->get('newPosition');
        $aisle = $request->get('aisle');
        $entry = $this->getDoctrine()->getRepository(Entry::class)->findById($id);
        $wishlist = $entry->getWishlist();
        if (!$wishlist->isSorted()) {
            if ($newPos > $oldPos) {
                foreach ($wishlist->getEntries() as $ent) {
                    if ($ent->getPositionInList() <= $newPos && $ent->getPositionInList() >= $oldPos) {
                        $ent->setPositionInList($ent->getPositionInList() - 1);
                        $this->getDoctrine()->getRepository(Entry::class)->save($ent);
                    }
                }
            }
            if ($newPos < $oldPos) {
                foreach ($wishlist->getEntries() as $ent) {
                    if ($ent->getPositionInList() >= $newPos && $ent->getPositionInList() <= $oldPos) {
                        $ent->setPositionInList($ent->getPositionInList() + 1);
                        $this->getDoctrine()->getRepository(Entry::class)->save($ent);
                    }
                }
            }
            $entry->setPositionInList($newPos);
        } else {
            $entry->getItem()->setAisle($aisle);
        }

        $this->getDoctrine()->getRepository(Entry::class)->save($entry);
        if ($wishlist->isSorted()) {
            return new JsonResponse(['result' => 1, 'sorted' => 1, 'aisles' => $this->serializeSortedEntries($entry->getWishlist()->getEntries(), $this->getDoctrine()->getRepository(Item::class)->findAisles())]);
        } else {
            return new JsonResponse(['result' => 1, 'sorted' => 0, 'entries' => $this->serializeEntries($entry->getWishlist()->getEntries())]);

        }
    }
}
