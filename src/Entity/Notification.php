<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NotificationRepository")
 */
class Notification
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="notifications")
     * @ORM\JoinColumn(name="target", referencedColumnName="id")
     * @var User
     */
    private $targetUser;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="source", referencedColumnName="id")
     */
    private $sourceUser;

    /**
     * @ORM\ManyToOne(targetEntity="Wishlist")
     * @ORM\JoinColumn(name="wishlist", referencedColumnName="id")
     */
    private $wishlist;
    /**
     *
     * @ORM\Column(unique=false, type="string")
     * @var string
     */
    private $type;

    /**
     *
     * @ORM\Column(type="datetime")
     * @var \Datetime
     */
    private $created;
    /**
     *
     * @ORM\Column(unique=false, type="string", nullable=true)
     * @var string
     */
    private $wishlistName;
    /**
     *
     * @ORM\Column(unique=false, type="string", nullable=true)
     * @var string
     */
    private $sourceUserName;
    /**
     * Notification constructor.
     */
    public function __construct()
    {
        $this->created=new \DateTime();
        $this->wishlistName="";
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getTargetUser(): User
    {
        return $this->targetUser;
    }

    /**
     * @param User $targetUser
     */
    public function setTargetUser(User $targetUser)
    {
        $this->targetUser = $targetUser;
    }

    /**
     * @return mixed
     */
    public function getSourceUser()
    {
        return $this->sourceUser;
    }

    /**
     * @param mixed $sourceUser
     */
    public function setSourceUser($sourceUser)
    {
        $this->sourceUser = $sourceUser;
        if($sourceUser)
        $this->sourceUserName=$sourceUser->getUsername();
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return \Datetime
     */
    public function getCreated(): \Datetime
    {
        return $this->created;
    }

    /**
     * @return mixed
     */
    public function getWishlist()
    {
        return $this->wishlist;
    }

    /**
     * @param mixed $wishlist
     */
    public function setWishlist($wishlist)
    {
        $this->wishlist = $wishlist;
    }

    /**
     * @return string
     */
    public function getWishlistName(): string
    {
        return $this->wishlistName;
    }

    /**
     * @param string $wishlistName
     */
    public function setWishlistName(string $wishlistName)
    {
        $this->wishlistName = $wishlistName;
    }

    /**
     * @return string
     */
    public function getSourceUserName(): string
    {
        return $this->sourceUserName;
    }

    /**
     * @param string $sourceUserName
     */
    public function setSourceUserName(string $sourceUserName)
    {
        $this->sourceUserName = $sourceUserName;
    }



}
