<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ItemRepository")
 */
class Item
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $compareName;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $aisle;

    /**
     *
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    private $deletable = true;

    /**
     *
     * @ORM\Column(type="integer")
     */
    private $count = 1;
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
        $this->setCompareName($name);
    }

    /**
     * @return bool
     */
    public function isDeletable(): bool
    {
        return $this->deletable;
    }


    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param mixed $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return mixed
     */
    public function getCompareName()
    {
        return $this->compareName;
    }

    /**
     * @param mixed $compareName
     */
    public function setCompareName($compareName)
    {
        setlocale(LC_ALL, 'czech');
        $this->compareName = iconv("utf-8", "us-ascii//TRANSLIT", $compareName);
    }

    /**
     * @return mixed
     */
    public function getAisle()
    {
        return $this->aisle;
    }

    /**
     * @param mixed $aisle
     */
    public function setAisle($aisle)
    {
        $this->aisle = $aisle;
    }


}
