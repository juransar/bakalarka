<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WishlistRepository")
 */
class Wishlist
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $name;

    /**
     *
     * @ORM\Column(type="datetime")
     * @var \Datetime
     */
    private $modified;

    /**
     *
     * @ORM\OneToMany(targetEntity="Entry", mappedBy="wishlist",cascade={"persist"})
     * @var Collection<Entry>
     */
    private $entries;

    /**
     *
     * @ORM\ManyToMany(targetEntity="User", mappedBy="invitedWishlists",cascade={"all"})
     *
     *  @var Collection<User>
     */
    public $users;


    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="wishlists")
     * @ORM\JoinColumn(name="owner", referencedColumnName="id")
     * @var User
     */
    private $owner;

    /**
     *
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    private $sorted = false;

    /**
     * Wishlist constructor.
     */
    public function __construct()
    {
        $this->users=new ArrayCollection();
        $this->entries=new ArrayCollection();
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return \Datetime
     */
    public function getModified(): \Datetime
    {
        return $this->modified;
    }

    /**
     * @param \Datetime $modified
     */
    public function setModified(\Datetime $modified)
    {
        $this->modified = $modified;
    }

    /**
     * @return Collection
     */
    public function getEntries(): Collection
    {
        return $this->entries;
    }

    /**
     * @param Entry $entry
     */
    public function addEntry($entry)
    {
        if ($this->entries->contains($entry)) {
            return;
        }
        $entry->setPositionInList(count($this->getEntries()));
        $this->entries->add($entry);
        $entry->setWishlist($this);
    }

    /**
     * @param Entry $entry
     */
    public function removeEntry($entry)
    {
        if (!$this->entries->contains($entry)) {
            return;
        }
        $this->entries->removeElement($entry);

    }

    /**
     * @return Collection
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    /**
     * @param User $user
     */
    public function addUser($user)
    {

        if ($this->users->contains($user)) {
            return;
        }
        $this->users->add($user);
        $user->addInvitedWishlist($this);
    }

    /**
     * @param User $user
     */
    public function removeUser($user)
    {
        if (!$this->users->contains($user)) {
            return;
        }
        $this->users->removeElement($user);
        $user->removeInvitedWishlist($this);

    }



    public function setEntries($entries){
        $this->entries=$entries;
    }

    /**
     * @return User
     */
    public function getOwner(): User
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     */
    public function setOwner(User $owner)
    {
        $this->owner = $owner;
        $owner->addWishlist($this);

    }

    /**
     * @return bool
     */
    public function isSorted()
    {
        return $this->sorted;
    }

    /**
     * @param bool $sorted
     */
    public function setSorted($sorted)
    {
        $this->sorted = $sorted;
    }


}
