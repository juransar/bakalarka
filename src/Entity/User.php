<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields="mail", message="Pod tímto e-mailem je už někdo zaregistrován")
 * @UniqueEntity(fields="username", message="Toto jméno už někdo používá")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     *
     * @ORM\Column(length=255, unique=true, type="string")
     * @var string
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $password;

    /**
     * @Assert\Length(max=4096)
     */
    private $plainPassword;

    /**
     *
     * @ORM\Column(unique=true, type="string")
     * @var string
     */
    private $mail;


    /**
     *
     * @ORM\OneToMany(targetEntity="Wishlist", mappedBy="owner",cascade={"persist"})
     * @var Collection<Wishlist>
     */
    private $wishlists;

    /**
     *
     * @ORM\ManyToMany(targetEntity="Wishlist", inversedBy="users",cascade={"persist"})
     * @ORM\JoinTable(
     *     name="users_wishlists",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="wishlist_id", referencedColumnName="id")
     * })
     * @var Collection<Wishlist>
     */
    private $invitedWishlists;

    /**
     * @ORM\ManyToMany(targetEntity="User")
     * @ORM\JoinTable(name="friends",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="friend_user_id", referencedColumnName="id")}
     *      )
     * @var Collection<User>
     */
    private $friends;

    /**
     *
     * @ORM\OneToMany(targetEntity="Notification", mappedBy="targetUser",cascade={"persist"})
     * @var Collection<Notification>
     */
    private $notifications;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $deletehash;

    /**
     *
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    private $emailnotifications = true;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->wishlists= new ArrayCollection();
        $this->invitedWishlists= new ArrayCollection();
        $this->friends= new ArrayCollection();
        $this->notifications= new ArrayCollection();
        $this->deletehash =  md5(uniqid());
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    /**
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param string $mail
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    /**
     * @return Collection
     */
    public function getWishlists()
    {
        return $this->wishlists;
    }

    /**
     * @param Wishlist $wishlist
     */
    public function addWishlist($wishlist)
    {
        if ($this->wishlists->contains($wishlist)) {
            return;
        }
        $this->wishlists->add($wishlist);
        $wishlist->setOwner($this);
    }

    /**
     * @param Wishlist $wishlist
     */
    public function removeWishlist($wishlist)
    {
        if (!$this->wishlists->contains($wishlist)) {
            return;
        }
        $this->wishlists->removeElement($wishlist);
        $wishlist->removeUser($this);

    }

    /**
     * @return Collection
     */
    public function getInvitedWishlists()
    {
        return $this->invitedWishlists;
    }

    /**
     * @param Wishlist $wishlist
     */
    public function addInvitedWishlist($wishlist)
    {
        if ($this->invitedWishlists->contains($wishlist)) {
            return;
        }
        $this->invitedWishlists->add($wishlist);
        $wishlist->addUser($this);
    }

    /**
     * @param Wishlist $wishlist
     */
    public function removeInvitedWishlist($wishlist)
    {
        if (!$this->invitedWishlists->contains($wishlist)) {
            return;
        }
        $this->invitedWishlists->removeElement($wishlist);
        $wishlist->removeUser($this);

    }
    /**
     * @return Collection
     */
    public function getFriends(): Collection
    {
        return $this->friends;
    }

    /**
     * @param User $friend
     */
    public function addFriend($friend)
    {
        if ($this->friends->contains($friend)) {
            return;
        }
        $this->friends->add($friend);
        $friend->addFriend($this);
    }

    /**
     * @param User $friend
     */
    public function removeFriend($friend)
    {

        if (!$this->friends->contains($friend)) {
            return;
        }
        $this->friends->removeElement($friend);
        $friend->removeFriend($this);

    }

    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
        ));
    }

    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            ) = unserialize($serialized);
    }

    public function getRoles()
    {
        return array('ROLE_USER');
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
    }


    /**
     * @return Collection
     */
    public function getNotifications()
    {
        return $this->notifications;
    }

    /**
     * @param Notification $notification
     */
    public function addNotification($notification)
    {
        if ($this->notifications->contains($notification)) {
            return;
        }
        $this->notifications->add($notification);
        $notification->setTargetUser($this);
    }

    /**
     * @param Notification $notification
     */
    public function removeNotification($notification)
    {
        if (!$this->notifications->contains($notification)) {
            return;
        }
        $this->notifications->removeElement($notification);
    }
    public function __toString()
    {
        return (string)$this->id;
    }

    /**
     * @return mixed
     */
    public function getDeletehash()
    {
        return $this->deletehash;
    }

    /**
     * @return bool
     */
    public function isEmailnotifications(): bool
    {
        return $this->emailnotifications;
    }

    /**
     * @param bool $emailnotifications
     */
    public function setEmailnotifications(bool $emailnotifications)
    {
        $this->emailnotifications = $emailnotifications;
    }


}
