<?php
/**
 * Created by PhpStorm.
 * User: sara
 * Date: 9.2.18
 * Time: 20:47
 */

namespace App\Repository;
use Doctrine\Common\Collections\Collection;

trait CRUD
{
    /**
     *
     * @param $entity
     */
    public function save($entity)
    {
        $this->isSupportedClass($entity);
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();

    }

    /**
     *
     * @param $entity
     */
    public function remove($entity)
    {
        $this->isSupportedClass($entity);
        $em = $this->getEntityManager();
        $em->remove($entity);
        $em->flush();

    }

    protected function isSupportedClass($entity)
    {
        $className = $this->getClassName();
        if (!$entity instanceof $className) {
            throw new \RuntimeException('Unsupported object');
        }
    }

    /**
     * @param int $id
     * @return object
     */
    public function findById($id)
    {
        return $this->find($id);
    }

    /**
     * @return Collection
     */
    public function findAllByClass()
    {
        return $this->findAll();
    }

}