<?php

namespace App\Repository;

use App\Entity\Item;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ItemRepository extends ServiceEntityRepository
{
    use CRUD;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Item::class);
    }

    public function findByName($name){
        return $this->findOneBy(["name"=>$name]);
    }

    public function findTypeahead($str){

        return $this->createQueryBuilder('i')
            ->select('i')
            ->where('i.name like :value AND i.deletable=0')->setParameter('value', "%".$str."%")
            ->getQuery()
            ->getResult()
            ;


    }

    public function findAisle($str){

        return $this->createQueryBuilder('i')
            ->select('i')
            ->where('i.name like :value')->setParameter('value', "%".$str."%")
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult()
            ;


    }
    public function findAisles(){
        $aisles=array_column($this->createQueryBuilder('i')
            ->select('i.aisle')
            ->distinct()
            ->getQuery()
            ->getResult(),'aisle');
        $aisles=array_diff($aisles,['Nezařazeno']);
        $aisles[]='Nezařazeno';
        return $aisles;
    }
}
