<?php

namespace App\Repository;

use App\Entity\Notification;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class NotificationRepository extends ServiceEntityRepository
{
    use CRUD;
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Notification::class);
    }

    public function findByWishlist($wishlist){
        return $this->createQueryBuilder('n')
            ->where('n.wishlist = :value')->setParameter('value', $wishlist->getId())
            ->getQuery()
            ->getResult()
            ;
    }

    public function findBySource($source){
        return $this->createQueryBuilder('n')
            ->where('n.sourceUser = :value')->setParameter('value', $source)
            ->andWhere('n.type =:FRIEND_R OR n.type =:WISHLIST_R')
            ->setParameter('FRIEND_R', "FRIEND_R")
            ->setParameter('WISHLIST_R', "WISHLIST_R")
            ->getQuery()
            ->getResult()
            ;
    }

    public function findByTarget($target){
        return $this->createQueryBuilder('n')
            ->where('n.targetUser = :value')->setParameter('value', $target)
            ->getQuery()
            ->getResult()
            ;
    }

    public function checkDuality(Notification $notification){
        $result=$this->createQueryBuilder('n')
            ->andWhere('n.sourceUser = :source AND n.targetUser = :target AND n.type = :type AND n.wishlist = :wishlist AND n.wishlistName = :wishlist_name')
            ->setParameter('source', $notification->getSourceUser())
            ->setParameter('target', $notification->getTargetUser())
            ->setParameter('type', $notification->getType())
            ->setParameter('wishlist', $notification->getWishlist())
            ->setParameter('wishlist_name', $notification->getWishlistName())
            ->getQuery()
            ->getOneOrNullResult();
        if ($result==null) return true;
        else return false;
    }

}
