<?php

namespace App\Repository;

use App\Entity\Wishlist;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class WishlistRepository extends ServiceEntityRepository
{
    use CRUD;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Wishlist::class);
    }

}
