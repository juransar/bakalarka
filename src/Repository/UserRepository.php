<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class UserRepository extends ServiceEntityRepository
{
    use CRUD;
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findByUsername($username) {
        return $this->findOneBy(['username' => $username]);
    }
    public function findByHash($hash) {
        return $this->findOneBy(['deletehash' => $hash]);
    }
    public function findTypeahead($str){

        return $this->createQueryBuilder('n')
            ->select('n')
            ->where('n.username like :value')->setParameter('value', "%".$str."%")
            ->getQuery()
            ->getResult()
            ;


    }
}
