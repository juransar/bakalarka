<?php
/**
 * Created by PhpStorm.
 * User: sara
 * Date: 7.3.18
 * Time: 21:37
 */

namespace App\Functionality;

use App\Entity\Notification;
use App\Entity\Wishlist;
use App\Entity\User;
use App\Entity\Entry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class WishlistsFunctionality
{
    protected $manager;
    protected $entryFunctionality;
    protected $notificationFunctionality;

    public function __construct(EntityManagerInterface $manager,EntryFunctionality $entryFunctionality,NotificationFunctionality $notificationFunctionality)
    {
        $this->manager = $manager;
        $this->entryFunctionality=$entryFunctionality;
        $this->notificationFunctionality=$notificationFunctionality;
    }

    public function makeWishlist($user,$wishlist){
        $user->addWishlist($wishlist);
        $wishlist->setModified(new \DateTime());
        $this->manager->getRepository(User::class)->save($user);
        $this->manager->getRepository(Wishlist::class)->save($wishlist);
    }

    public function removeWishlist(Wishlist $wishlist){
        foreach ($this->manager->getRepository(Notification::class)->findByWishlist($wishlist) as $notification){
            $notification->getTargetUser()->removeNotification($notification);
            $this->manager->getRepository(User::class)->save($notification->getTargetUser());
            $this->manager->getRepository(Notification::class)->remove($notification);

        }

        /* @var $user User */
        foreach ($wishlist->getUsers() as $user){
            $user->removeInvitedWishlist($wishlist);
            $this->notificationFunctionality->removedFromWishlist($wishlist->getOwner(),$user,$wishlist);
            $this->manager->getRepository(User::class)->save($user);
        }
        /* @var $entry Entry */
        foreach ($wishlist->getEntries() as $entry){
            $this->entryFunctionality->removeEntry($entry);
        }
        $wishlist->getOwner()->removeWishlist($wishlist);
        $this->manager->getRepository(User::class)->save($wishlist->getOwner());
        $this->manager->getRepository(Wishlist::class)->remove($wishlist);

    }

    public function removeUserFromWishlist(User $user,Wishlist $wishlist){
        $wishlist->removeUser($user);
        $this->notificationFunctionality->removedFromWishlist($wishlist->getOwner(),$user,$wishlist);
        $this->manager->getRepository(Wishlist::class)->save($wishlist);
        $this->manager->getRepository(User::class)->save($user);


    }

    public function removeIvitedWishlist(User $user,Wishlist $wishlist){
        $wishlist->removeUser($user);
        $this->notificationFunctionality->removedFromIvitedWishlist($user,$wishlist->getOwner(),$wishlist);
        $this->manager->getRepository(Wishlist::class)->save($wishlist);
        $this->manager->getRepository(User::class)->save($user);


    }
}