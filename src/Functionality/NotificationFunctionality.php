<?php
/**
 * Created by PhpStorm.
 * User: sara
 * Date: 7.3.18
 * Time: 21:02
 */

namespace App\Functionality;

use App\Entity\Notification;
use App\Entity\User;
use App\Entity\Wishlist;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class NotificationFunctionality
{
    protected $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }
    public function acceptFriendRequest($notification){
        $source=$notification->getSourceUser();
        $target=$notification->getTargetUser();
        $target->addFriend($source);
        $target->removeNotification($notification);
        $this->manager->getRepository(Notification::class)->remove($notification);
        $notification=new Notification();
        $notification->setTargetUser($source);
        $notification->setSourceUser(null);
        $notification->setSourceUserName($target->getUsername());
        $notification->setType("FRIEND_A");
        $source->addNotification($notification);
        $this->manager->getRepository(Notification::class)->save($notification);
        $this->manager->getRepository(User::class)->save($target);
        $this->manager->getRepository(User::class)->save($source);

    }
    public function dennyFriendRequest($notification){
        $source=$notification->getSourceUser();
        $target=$notification->getTargetUser();
        $target->removeNotification($notification);
        $this->manager->getRepository(Notification::class)->remove($notification);
        $notification=new Notification();
        $notification->setTargetUser($source);
        $notification->setSourceUser(null);
        $notification->setSourceUserName($target->getUsername());
        $notification->setType("FRIEND_D");
        $source->addNotification($notification);
        $this->manager->getRepository(Notification::class)->save($notification);
        $this->manager->getRepository(User::class)->save($target);
        $this->manager->getRepository(User::class)->save($source);

    }
    public function makeFriendRequest($source,$target){
        $notification=new Notification();
        $notification->setSourceUser($source);
        $notification->setTargetUser($target);
        $notification->setType("FRIEND_R");
        if($this->manager->getRepository(Notification::class)->checkDuality($notification)){
            $target->addNotification($notification);
            $this->manager->getRepository(Notification::class)->save($notification);
            $this->manager->getRepository(User::class)->save($target);
        }

    }

    public function makeInvitationRequest($source,$targets,$wishlist){
        foreach ($targets as $target) {
            $notification = new Notification();
            $notification->setSourceUser($source);
            $notification->setTargetUser($target);
            $notification->setType("WISHLIST_R");
            $notification->setWishlist($wishlist);
            if($this->manager->getRepository(Notification::class)->checkDuality($notification)) {

                $target->addNotification($notification);
                $this->manager->getRepository(Notification::class)->save($notification);
                $this->manager->getRepository(User::class)->save($target);
            }
        }
    }

    public function acceptWishlistRequest($notification){
        $source=$notification->getSourceUser();
        $target=$notification->getTargetUser();
        $wishlist=$notification->getWishlist();
        $target->addInvitedWishlist($wishlist);
        $target->removeNotification($notification);
        $this->manager->getRepository(Notification::class)->remove($notification);
        $notification=new Notification();
        $notification->setTargetUser($source);
        $notification->setSourceUser(null);
        $notification->setSourceUserName($target->getUsername());
        $notification->setType("WISHLIST_A");
        $notification->setWishlist($wishlist);
        $source->addNotification($notification);
        $this->manager->getRepository(Notification::class)->save($notification);
        $this->manager->getRepository(User::class)->save($target);
        $this->manager->getRepository(User::class)->save($target);
        $this->manager->getRepository(Wishlist::class)->save($wishlist);

    }
    public function dennyWishlistRequest($notification){
        $source=$notification->getSourceUser();
        $target=$notification->getTargetUser();
        $wishlist=$notification->getWishlist();
        $target->removeNotification($notification);
        $this->manager->getRepository(Notification::class)->remove($notification);
        $notification=new Notification();
        $notification->setTargetUser($source);
        $notification->setSourceUser(null);
        $notification->setSourceUserName($target->getUsername());
        $notification->setType("WISHLIST_D");
        $notification->setWishlist($wishlist);
        $source->addNotification($notification);
        $this->manager->getRepository(Notification::class)->save($notification);
        $this->manager->getRepository(User::class)->save($target);
        $this->manager->getRepository(User::class)->save($source);
        $this->manager->getRepository(Wishlist::class)->save($wishlist);

    }
    public function removedFromWishlist($source,$target,$wishlist){
        $notification=new Notification();
        $notification->setSourceUserName($source->getUsername());
        $notification->setTargetUser($target);
        $notification->setWishlistName($wishlist->getName());
        $notification->setType("IWISHLIST_D");
        $this->manager->getRepository(Notification::class)->save($notification);
        $this->manager->getRepository(User::class)->save($target);
    }
    public function removedFromIvitedWishlist($source,$target,$wishlist){
        $notification=new Notification();
        $notification->setSourceUserName($source->getUsername());
        $notification->setTargetUser($target);
        $notification->setWishlist($wishlist);
        $notification->setType("IWISHLIST_UD");
        $this->manager->getRepository(Notification::class)->save($notification);
        $this->manager->getRepository(User::class)->save($target);
    }
    public function removedFromFriends($source,$target){
        $notification=new Notification();
        $notification->setSourceUserName($source->getUsername());
        $notification->setTargetUser($target);
        $notification->setType("UFRIEND_R");
        $this->manager->getRepository(Notification::class)->save($notification);
        $this->manager->getRepository(User::class)->save($target);
    }

    public function removeNotification($notification){
        if ($notification->getType()=="FRIEND_R") {
            $this->dennyFriendRequest($notification);
        }
        else if ($notification->getType()=="WISHLIST_R") {
            $this->dennyWishlistRequest($notification);
        }
        $notification->getTargetUser()->removeNotification($notification);
        $this->manager->getRepository(Notification::class)->remove($notification);
        $this->manager->getRepository(User::class)->save($notification->getTargetUser());
    }

}