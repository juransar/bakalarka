<?php
/**
 * Created by PhpStorm.
 * User: sara
 * Date: 9.3.18
 * Time: 22:38
 */

namespace App\Functionality;

use App\Entity\Entry;
use App\Entity\Wishlist;
use App\Entity\Item;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;

class EntryFunctionality
{
    protected $manager;
    protected $directory;

    public function __construct(EntityManagerInterface $manager,$directory)
    {
        $this->manager = $manager;
        $this->directory=$directory;
    }

    public function saveEntry($entry,$name,$file,$wishlist){
        if ($file != null) {

            $fileName = md5(uniqid()) . '.' . $file->guessExtension();
            $file->move(
                $this->directory,
                $fileName
            );
            $entry->setImage($fileName);

        }
        $item = $this->manager->getRepository(Item::class)->findByName($name);
        if (!$item) {
            $item = new Item();
            $item->setAisle("Nezařazeno");
            $searchable=explode(" ",$name);
            foreach ($searchable as $search){
                $found=$this->manager->getRepository(Item::class)->findAisle($search);
                if($found){
                    $item->setAisle($found->getAisle());
                    break;
                }

            }

            $item->setName($name);
            $this->manager->getRepository(Item::class)->save($item);
        }
        else {
            $item->setCount($item->getCount()+1);
        }

        $entry->setItem($item);
        $wishlist->addEntry($entry);
        $this->manager->getRepository(Wishlist::class)->save($wishlist);
        $this->manager->getRepository(Entry::class)->save($entry);

    }
    public function transferEntries($entries,$wishlist){
        foreach ($entries as $entry){
            $newentry=clone $entry;
            $entry->getItem()->setCount($entry->getItem()->getCount()+1);
            if($entry->getImage()!=null) {
                $fileSystem=new Filesystem();
                $filename = $entry->getImage();
                $newfilename = md5(uniqid()) . '.' . pathinfo($filename)['extension'];
                $fileSystem->copy( $this->directory."/".$filename, $this->directory."/".$newfilename);
                $entry->setImage($newfilename);
            }
            $newentry->setChecked(false);
            $wishlist->addEntry($newentry);
            $this->manager->getRepository(Entry::class)->save($newentry);

        }
        $this->manager->getRepository(Wishlist::class)->save($wishlist);
    }

    public function changeEntry($entry){
        $entry->setChecked(!$entry->getChecked());
        $this->manager->getRepository(Entry::class)->save($entry);
    }

    public function removeEntry($entry){
        $wishlist=$entry->getWishlist();
        if ($entry->getImage() != null) {
            $filesystem = new Filesystem();
            $filesystem->remove($this->directory . "/" . $entry->getImage());
        }
        $entry->getItem()->setCount( $entry->getItem()->getCount()-1);

        $this->manager->getRepository(Entry::class)->remove($entry);
        if($entry->getItem()->getCount()==0 && $entry->getItem()->isDeletable()) {
            $this->manager->getRepository(Item::class)->remove($entry->getItem());

        }
        $wishlist->removeEntry($entry);
        foreach ($wishlist->getEntries() as $ent){
            if ($ent->getPositionInList() > $entry->getPositionInList()){
                $ent->setPositionInList($ent->getPositionInList()-1) ;
                $this->manager->getRepository(Entry::class)->save($ent);
            }
        }
        $this->manager->getRepository(Wishlist::class)->save($wishlist);
    }

}