<?php
/**
 * Created by PhpStorm.
 * User: sara
 * Date: 21.3.18
 * Time: 16:47
 */

namespace App\Functionality;

use App\Entity\Notification;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
class UserFunctionality
{
    protected $manager;
    protected $wishlistFunctionality;
    protected $notificationFunctionality;

    public function __construct(EntityManagerInterface $manager,WishlistsFunctionality $wishlistsFunctionality,NotificationFunctionality $notificationFunctionality)
    {
        $this->manager = $manager;
        $this->wishlistFunctionality=$wishlistsFunctionality;
        $this->notificationFunctionality=$notificationFunctionality;

    }

    public function removeUser($user){
        //remove notifications
        $notifications=$this->manager->getRepository(Notification::class)->findBySource($user);
        foreach ($notifications as $notification){
            $this->manager->getRepository(Notification::class)->remove($notification);
        }
        $notifications=$this->manager->getRepository(Notification::class)->findByTarget($user);
        foreach ($notifications as $notification){
            $this->notificationFunctionality->removeNotification($notification);
        }
        foreach ($user->getWishlists() as $wishlist){
            $this->wishlistFunctionality->removeWishlist($wishlist);
        }
        foreach ($user->getInvitedWishlists() as $wishlist){
            $this->wishlistFunctionality->removeIvitedWishlist($user,$wishlist);
        }
        //remove wishlists and friends
        foreach ($user->getFriends() as $friend){

            $friend->removeFriend($user);
            $this->notificationFunctionality->removedFromFriends($user,$friend);
            $this->manager->getRepository(User::class)->save($friend);


        }
        $this->manager->getRepository(User::class)->remove($user);
    }

    public function removeFriend($source,$target){
        foreach ($target->getInvitedWishlists() as $wishlist){
            if($wishlist->getOwner()==$source){
                $this->wishlistFunctionality->removeUserFromWishlist($target,$wishlist);
            }
        }
        foreach ($source->getInvitedWishlists() as $wishlist){
            if($wishlist->getOwner()==$target){
                $this->wishlistFunctionality->removeUserFromWishlist($source,$wishlist);
            }
        }
        foreach ($target->getNotifications() as $notification){
            if($notification->getType()=="WISHLIST_R" && $notification->getSourceUser()==$source){
                $this->notificationFunctionality->removeNotification($notification);
            }
        }
        foreach ($source->getNotifications() as $notification){
            if($notification->getType()=="WISHLIST_R" && $notification->getSourceUser()==$target){
                $this->notificationFunctionality->removeNotification($notification);
            }
        }
        $source->removeFriend($target);
        $this->notificationFunctionality->removedFromFriends($source,$target);
        $this->manager->getRepository(User::class)->save($source);
        $this->manager->getRepository(User::class)->save($target);
    }

}