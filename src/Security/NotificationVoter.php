<?php
/**
 * Created by PhpStorm.
 * User: sara
 * Date: 22.3.18
 * Time: 18:22
 */
namespace App\Security;

use App\Entity\Notification;
use App\Entity\User;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class NotificationVoter extends Voter
{
    const EDIT = 'edit';
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(self::EDIT))) {
            return false;
        }
        if (!$subject instanceof Notification) {
            return false;
        }
        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }
        $notification = $subject;

        return $this->canEdit($notification, $user);

    }

    private function canEdit(Notification $notification, User $user)
    {
        // this assumes that the data object has a getOwner() method
        // to get the entity of the user who owns this data object
        return ($user === $notification->getTargetUser());
    }



}