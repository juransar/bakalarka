<?php
/**
 * Created by PhpStorm.
 * User: sara
 * Date: 22.3.18
 * Time: 18:22
 */
namespace App\Security;

use App\Entity\User;
use App\Entity\Wishlist;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class UserVoter extends Voter
{
    const DELETE = 'remove';
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(self::DELETE))) {
            return false;
        }
        if (!$subject instanceof User) {
            return false;
        }
        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }
        $target = $subject;

        switch ($attribute) {
            case self::DELETE:
                return $this->canDelete($target, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canDelete(User $target, User $user)
    {
        // this assumes that the data object has a getOwner() method
        // to get the entity of the user who owns this data object
        return in_array($user, $target->getFriends()->getValues());
    }


}