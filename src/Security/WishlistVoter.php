<?php
/**
 * Created by PhpStorm.
 * User: sara
 * Date: 22.3.18
 * Time: 18:22
 */
namespace App\Security;

use App\Entity\User;
use App\Entity\Wishlist;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class WishlistVoter extends Voter
{
    const VIEW = 'view';
    const DELETE = 'delete';
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(self::VIEW, self::DELETE))) {
            return false;
        }
        if (!$subject instanceof Wishlist) {
            return false;
        }
        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }
        $wishlist = $subject;

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($wishlist, $user);
            case self::DELETE:
                return $this->canDelete($wishlist, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canDelete(Wishlist $wishlist, User $user)
    {
        // this assumes that the data object has a getOwner() method
        // to get the entity of the user who owns this data object
        return $user === $wishlist->getOwner();
    }

    private function canView(Wishlist $wishlist, User $user)
    {
        // this assumes that the data object has a getOwner() method
        // to get the entity of the user who owns this data object
        if ($this->canDelete($wishlist,$user)) return true;
        return in_array($user, $wishlist->getUsers()->getValues());
    }

}