<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180407160839 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE wishlist (id INT AUTO_INCREMENT NOT NULL, owner INT DEFAULT NULL, name VARCHAR(64) DEFAULT NULL, modified DATETIME NOT NULL, sorted TINYINT(1) NOT NULL, INDEX IDX_9CE12A31CF60E67C (owner), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notification (id INT AUTO_INCREMENT NOT NULL, target INT DEFAULT NULL, source INT DEFAULT NULL, wishlist INT DEFAULT NULL, type VARCHAR(255) NOT NULL, created DATETIME NOT NULL, wishlist_name VARCHAR(255) DEFAULT NULL, source_user_name VARCHAR(255) DEFAULT NULL, INDEX IDX_BF5476CA466F2FFC (target), INDEX IDX_BF5476CA5F8A7F73 (source), INDEX IDX_BF5476CA9CE12A31 (wishlist), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE entry (id INT AUTO_INCREMENT NOT NULL, wishlist_id INT DEFAULT NULL, item_id INT DEFAULT NULL, checked TINYINT(1) NOT NULL, image VARCHAR(255) DEFAULT NULL, link VARCHAR(255) DEFAULT NULL, price NUMERIC(10, 2) DEFAULT NULL, amount VARCHAR(255) DEFAULT NULL, position_in_list INT DEFAULT NULL, INDEX IDX_2B219D70FB8E54CD (wishlist_id), INDEX IDX_2B219D70126F525E (item_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE item (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(64) DEFAULT NULL, compare_name VARCHAR(64) DEFAULT NULL, aisle VARCHAR(64) DEFAULT NULL, deletable TINYINT(1) NOT NULL, count INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(255) NOT NULL, password VARCHAR(64) DEFAULT NULL, mail VARCHAR(255) NOT NULL, deletehash VARCHAR(255) NOT NULL, emailnotifications TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), UNIQUE INDEX UNIQ_8D93D6495126AC48 (mail), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users_wishlists (user_id INT NOT NULL, wishlist_id INT NOT NULL, INDEX IDX_2C3154FAA76ED395 (user_id), INDEX IDX_2C3154FAFB8E54CD (wishlist_id), PRIMARY KEY(user_id, wishlist_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE friends (user_id INT NOT NULL, friend_user_id INT NOT NULL, INDEX IDX_21EE7069A76ED395 (user_id), INDEX IDX_21EE706993D1119E (friend_user_id), PRIMARY KEY(user_id, friend_user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE wishlist ADD CONSTRAINT FK_9CE12A31CF60E67C FOREIGN KEY (owner) REFERENCES user (id)');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CA466F2FFC FOREIGN KEY (target) REFERENCES user (id)');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CA5F8A7F73 FOREIGN KEY (source) REFERENCES user (id)');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CA9CE12A31 FOREIGN KEY (wishlist) REFERENCES wishlist (id)');
        $this->addSql('ALTER TABLE entry ADD CONSTRAINT FK_2B219D70FB8E54CD FOREIGN KEY (wishlist_id) REFERENCES wishlist (id)');
        $this->addSql('ALTER TABLE entry ADD CONSTRAINT FK_2B219D70126F525E FOREIGN KEY (item_id) REFERENCES item (id)');
        $this->addSql('ALTER TABLE users_wishlists ADD CONSTRAINT FK_2C3154FAA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE users_wishlists ADD CONSTRAINT FK_2C3154FAFB8E54CD FOREIGN KEY (wishlist_id) REFERENCES wishlist (id)');
        $this->addSql('ALTER TABLE friends ADD CONSTRAINT FK_21EE7069A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE friends ADD CONSTRAINT FK_21EE706993D1119E FOREIGN KEY (friend_user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CA9CE12A31');
        $this->addSql('ALTER TABLE entry DROP FOREIGN KEY FK_2B219D70FB8E54CD');
        $this->addSql('ALTER TABLE users_wishlists DROP FOREIGN KEY FK_2C3154FAFB8E54CD');
        $this->addSql('ALTER TABLE entry DROP FOREIGN KEY FK_2B219D70126F525E');
        $this->addSql('ALTER TABLE wishlist DROP FOREIGN KEY FK_9CE12A31CF60E67C');
        $this->addSql('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CA466F2FFC');
        $this->addSql('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CA5F8A7F73');
        $this->addSql('ALTER TABLE users_wishlists DROP FOREIGN KEY FK_2C3154FAA76ED395');
        $this->addSql('ALTER TABLE friends DROP FOREIGN KEY FK_21EE7069A76ED395');
        $this->addSql('ALTER TABLE friends DROP FOREIGN KEY FK_21EE706993D1119E');
        $this->addSql('DROP TABLE wishlist');
        $this->addSql('DROP TABLE notification');
        $this->addSql('DROP TABLE entry');
        $this->addSql('DROP TABLE item');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE users_wishlists');
        $this->addSql('DROP TABLE friends');
    }
}
