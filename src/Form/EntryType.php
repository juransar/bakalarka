<?php
/**
 * Created by PhpStorm.
 * User: sara
 * Date: 15.2.18
 * Time: 23:33
 */

namespace App\Form;

use App\Entity\Entry;
use App\Entity\Wishlist;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
class EntryType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array('label' => 'Nová položka
            ', 'mapped' => false,'constraints' => array(
                new NotBlank(),
                new Length(array('min' => 3,'max'=>30)),
            ),))
            ->add('amount', TextType::class, array('label' => 'Množství
            ', 'required' => false))
            ->add('price', NumberType::class, array('label' => 'Předpokládaná cena
            ', 'required' => false))
            ->add('link',UrlType::class,['label' => 'Odkaz','required' => false])
            ->add('image', FileType::class, array('label' => 'Obrázek', 'required' => false,'data_class' => null))
            ->add('Přidat', SubmitType::class);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Entry::class,
            'wishlist' => null,
        ));

    }
}