<?php
/**
 * Created by PhpStorm.
 * User: sara
 * Date: 8.3.18
 * Time: 11:02
 */

namespace App\Form;

use App\Entity\User;
use App\Entity\Wishlist;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
class InvitationType extends AbstractType
{
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $wishlist = $options['wishlist'];
        $user = $this->tokenStorage->getToken()->getUser();


        $builder
            ->add('Přidat', SubmitType::class);

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($user,$wishlist) {
                $form = $event->getForm();

                $formOptions = array(
                    'class'         => User::class,
                    'choice_label' => 'username',
                    'mapped' => false,
                    'label' => 'Vyberte přátele',
                    'required' => false,
                    'multiple' => true,
                    'expanded' => true,
                    'choices'=> array_udiff($user->getFriends()->getValues(),array_merge( $wishlist->getUsers()->getValues(),[$wishlist->getOwner()]), function ($obj_a, $obj_b){
                        return $obj_a->getId() - $obj_b->getId();
                    }),

                );

                // create the field, this is similar the $builder->add()
                // field name, field type, data, options
                $form->add('friends', EntityType::class, $formOptions);
            }
        );

    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            "data_class"=>null,
            'wishlist' => null,

        ));
        $resolver->setRequired('wishlist');
        $resolver->setAllowedTypes('wishlist', Wishlist::class);
    }


}