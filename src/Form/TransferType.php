<?php
/**
 * Created by PhpStorm.
 * User: sara
 * Date: 15.2.18
 * Time: 23:33
 */

namespace App\Form;

use App\Entity\Wishlist;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormInterface;

class TransferType extends AbstractType
{
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $wishlists = $options['wishlists'];


        $builder->add('wishlists', EntityType::class, array(
            'class'         => Wishlist::class,
            'choice_label'  => 'name',
            'choices' => $wishlists,
            'mapped'=>false,
            'label' => 'Vyberte seznam',
            'required' =>false,
        ))
        ->add('Přidat', SubmitType::class);

        $formModifier = function (FormInterface $form, Wishlist $wishlist = null) {
            $entries = null === $wishlist ? array() : $wishlist->getEntries();

            $form->add('entries', EntityType::class, array(
                'class' => 'App\Entity\Entry',
                'multiple' => true,
                'expanded' => true,
                'choice_label' =>'item.name',
                'choices' => $entries,
                'mapped' =>false,
                'label' =>'Položky'
            ));
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                // this would be your entity, i.e. SportMeetup
                $data = $event->getData();

                $formModifier($event->getForm(), null);
            }
        );
        $builder->get('wishlists')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                // It's important here to fetch $event->getForm()->getData(), as
                // $event->getData() will get you the client data (that is, the ID)
                $wishlist = $event->getForm()->getData();

                // since we've added the listener to the child, we'll have to pass on
                // the parent to the callback functions!
                $formModifier($event->getForm()->getParent(), $wishlist);
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Wishlist::class,
            'wishlists' => null,

        ));
    }
}