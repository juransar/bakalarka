<?php
/**
 * Created by PhpStorm.
 * User: sara
 * Date: 14.2.18
 * Time: 22:08
 */
namespace App\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class LoginForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('_username', TextType::class, array('label' => 'Uživatelské jméno'))
            ->add("_password",PasswordType::class,array('label' => 'Heslo'))
            ->add('Odeslat', SubmitType::class)
        ;

    }
}