<?php
/**
 * Created by PhpStorm.
 * User: sara
 * Date: 21.3.18
 * Time: 23:47
 */

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType as MailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mail', MailType::class, array('label' => 'E-Mail','invalid_message' => 'Vyplňte platnou e-mailovou adresu'))
            ->add('Submit', SubmitType::class,['label'=> 'Změnit email'])
        ;
    }


public function configureOptions(OptionsResolver $resolver)
{
    $resolver->setDefaults(array(
        'data_class' => User::class,
    ));
}
}